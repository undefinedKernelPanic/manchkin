function GameFieldController($scope, $element, $attrs, api) {
  var ctrl = this;

  ctrl.handleAction = function(q) {
    //api.sendAction().then(function success (response) {
    //  console.log('success');
    //}, function error(response) {
    //  console.log('error')
    //});


    console.log(q);
    var index = null;
    ctrl.owner.cards.forEach(function (item, i) {
      if (item.id == q.senderId) {
        index = i;
      }
    });

    if (index > -1) {
      ctrl.owner.cards.splice(index, 1);
    }
  };

  $scope.handleDrop = function(senderId, recipientId) {
    ctrl.handleAction({recipientId: null, senderId: senderId, type: 'dummy playground'});
  };

  $scope.players = api.players;

  //$scope.$watch('players', function (){
  //  var total = 0;
  //  $scope.players.forEach(function(i){
  //    total += i.val;
  //  });
  //  ctrl.sum = total;
  //}, true)

  ctrl.owner = api.owner;
}

angular.module('Manchkin').component('gameField', {
  templateUrl: 'gameField.html',
  controller: GameFieldController
});