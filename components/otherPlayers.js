function OtherPlayersController($scope, $element, $attrs) {
    var ctrl = this;
    //console.log(ctrl);

    $scope.handleDrop = function(senderId, recipientId) {
        ctrl.handleAction({action: {recipientId: recipientId, senderId: senderId, type: 'dummy player'}});
    };

    y = window.screen.availHeight;
    x = window.screen.availWidth;
    rx = x * 0.5;
    ry = y * 0.8;
    p = ctrl.players.length;

    //console.log(x);
    //console.log(y);
    //console.log(getPlayerPlaces(x, y, rx, ry, p));
    $scope.playersPosition = (getPlayerPlaces(x, y, rx, ry, p));

    function getPlayerPlaces(x, y, rx, ry, p) {
        // var s = x / p * 75;


        var horizontAngle = 10;
        var availableSector = 180 - 2 * horizontAngle;
        var sectorPerPlayer = availableSector / (p + 1);
        //console.log(sectorPerPlayer);
        //var e = Math.sqrt(1-(y*y/4) / (x*x/4));
        //console.log(e);
        var result = [];
        for (i = 0; i < p; i++) {
            var xd = rx * Math.cos((horizontAngle + sectorPerPlayer + i * sectorPerPlayer) * Math.PI / 180);
            var yd = ry * Math.sin((horizontAngle + sectorPerPlayer + i * sectorPerPlayer) * Math.PI / 180);
            // result.push({x: Mat1
            // h.abs(2 * xd) + 'px', y: (y - yd) + 'px', color:'red'});
            var xx = x/2+ xd - 75/2;
            var yy = y-yd-(y-ry)/2 -20-50/2;
            result.push({id: i, x: xx + 'px', y:  yy + 'px', color:'red', deg: Math.atan(xd/yd*(ry/rx))});
        }
        return result;
    }
}


angular.module('Manchkin').component('otherPlayers', {
    templateUrl: 'otherPlayers.html',
    controller: OtherPlayersController,
    bindings: {
        players: '<',
        handleAction: '&'
    }
});