var app = angular.module('Manchkin', []);


app.controller('DragDropCtrl', function($scope) {
    $scope.handleDrop = function() {
    	console.log($scope);
        alert('Item has been dropped');
    }
});

app.directive('rotate', function() {
	return {
		link: function(scope, element, attrs) {
			// watch the degrees attribute, and update the UI when it changes
			scope.$watch(attrs.degrees, function(rotateDegrees) {
				//console.log(rotateDegrees);
				//transform the css to rotate based on the new rotateDegrees
				element.css({
					'-moz-transform': 'rotate(' + rotateDegrees + 'rad)',
					'-webkit-transform': 'rotate(' + rotateDegrees + 'rad)',
					'-o-transform': 'rotate(' + rotateDegrees + 'rad)',
					'-ms-transform': 'rotate(' + rotateDegrees + 'rad)'
				});
			});
		}
	}
});


app.directive('draggable', function() {
	return function(scope, element) {
		var el = element[0];

		el.draggable = true;

		el.addEventListener(
			'dragstart',
			function(e) {
				e.dataTransfer.effectAllowed = 'move';
				e.dataTransfer.setData('Text', this.id);
				this.classList.add('drag');
				return false;
			},
			false
		);

		el.addEventListener(
			'dragend',
			function(e) {
				this.classList.remove('drag');
				return false;
			},
			false
		);
	}
});


app.directive('droppable', function() {
	return {
		scope: {
			drop: '&', // parent
		  bin: '=' // bi-directional scope	
		},
		link: function(scope, element) {
			var el = element[0];

			el.addEventListener(
		    'dragover',
		    function(e) {
		        e.dataTransfer.dropEffect = 'move';
		        // allows us to drop
		        if (e.preventDefault) e.preventDefault();
		        this.classList.add('over');
		        return false;
			  },
			  false
			);

			el.addEventListener(
			    'dragleave',
			    function(e) {
			        this.classList.remove('over');
			        return false;
			    },
			    false
			);

			el.addEventListener(
        'drop',
        function(e) {
          // Stops some browsers from redirecting.
          if (e.stopPropagation) e.stopPropagation();
          
          this.classList.remove('over');
          
          var binId = this.id;
          var item = document.getElementById(e.dataTransfer.getData('Text'));
          // this.appendChild(item);
          // call the passed drop function
          scope.$apply(function(scope) {
            var fn = scope.drop();
            if ('undefined' !== typeof fn) {
              fn(item.id, binId);
            }
          });
          
          return false;
        },
        false
      );



		}
	}
})